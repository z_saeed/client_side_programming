/**************************************
	TITLE: loopingThroughArrays.js
	AUTHOR: Zaid Saeed
	CREATE DATE: 10/1
	PURPOSE: loopingThroughArrays lab
	LAST MODIFIED ON: 10/3
	LAST MODIFIED BY: Zaid Saeed
	MODIFICATION HISTORY: Add funtionality
***************************************/


$(document).ready(function(){
			
	alert("Hoist the secret message ye scurvy dawgs!");
	
	//Your code here
	var strUserInput;
	
	var boolBreak = false;
	while (!boolBreak) {
		
		strUserInput = prompt("Please input a valid character.")
		var regex=/^[a-zA-Z]+$/;
		
		if (strUserInput.length == 1 && strUserInput.match(regex)) {
			boolBreak = true;
		}		
	}
	
	var intAsciiValue = parseAscii(strUserInput);
	var strBinary = parseBin(intAsciiValue);
	
	var strBinaryArray = strBinary.split("");
	console.log(strBinaryArray);
	
	var element = document.getElementById("binaryText")
	
	for (var intBinIndex in strBinaryArray) {
		if (strBinaryArray[intBinIndex] == '1') {
			element.textContent += "true ";
		} else {
			element.textContent += "false ";
		}
	}
	
	/*****			
	Purpose: This function takes a character and returns a corresponding Ascii number.			
	Parameters: single character / letter		
	Return: integer representing an ascii value
	*****/
	function parseAscii(chrCharacter)
	{
		var intAscii = chrCharacter.charCodeAt(0);
		return intAscii;
	}
	
	/*****			
	Purpose: Takes the ascii value and returns a string with the corresponding binary value.			
	Parameters: single integer representing an ascii value	
	Return: binary, base 2 representation of the number passed to this function
	*****/
	function parseBin(intAscii)
	{
		var strBin = parseInt(intAscii, 10).toString(2);
		if(strBin.length < 8)
		{
			var intPlaceHolders = 8 - strBin.length;
			for(var i = 0; i < intPlaceHolders; i++)
			{
				strBin = "0" + strBin;
			}
			
		}
		return strBin;
	}
});