/*
TITLE: conversions.js
AUTHOR: Zaid Saeed
PURPOSE: Conversions Lab Assignment - Prompt User for Information
ORIGINALLY CREATED ON: 08/28/2018
LAST MODIFIED ON: 08/28/2018
LAST MODIFIED BY: Zaid Saeed
MODIFICATION HISTORY: Initial Creation
*/

$(document).ready(function () {

    var strName = "";
    var strAskName = "What is your pirate name?";
    var strDefaultName = "Captain Jack Cardinal";
    
    strName = prompt(strAskName,strDefaultName);
    
    var strDoubloons = "";    
    var strAskPlunder = "What be the number of gold doubloons ye plundered?";
    var strAskPlunderDefault = "1";
    
    strDoubloons = prompt(strAskPlunder, strAskPlunderDefault);
    
    var intDoubloons;
    const intConversionRate = 287;
    
    intDoubloons = parseInt(strDoubloons);
    
    var intProfit;
    
    intProfit = intDoubloons * intConversionRate;
    
    var strProfit = "";
    
    strProfit = intProfit.toString();
    
    var strOutput = strName + " ye have plundered " + strDoubloons + " gold doubloons. This gives ye a profit of $" + strProfit + ". ARRRR!!!!";
    
    var outputElement = document.getElementById("output");
    outputElement.textContent = strOutput;
    

}); // end of $(document).ready()
