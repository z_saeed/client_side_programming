/*
TITLE: multiwrite.js
AUTHOR: Zaid Saeed
PURPOSE: Multi Write Lab Assignment - document.write()
ORIGINALLY CREATED ON: 08/27/2018
LAST MODIFIED ON: 08/27/2018
LAST MODIFIED BY: Zaid Saeed
MODIFICATION HISTORY: Initial Creation
*/

$(document).ready(function(){
    document.write("ARRR, Mi name be Captain Zaid Saeed.")
});
