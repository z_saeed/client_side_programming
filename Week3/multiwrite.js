/*
TITLE: multiwrite.js
AUTHOR: Zaid Saeed
PURPOSE: Multi Write Lab Assignment - textContent, innerHTML, document.write
ORIGINALLY CREATED ON: 08/27/2018
LAST MODIFIED ON: 08/27/2018
LAST MODIFIED BY: Zaid Saeed
MODIFICATION HISTORY: Initial Creation
*/

$(document).ready(function () {

    //TEXTCONTENT
    var strMessageTC = "Arrr, Mi major be computer science."
    var elementTC = document.getElementById("miMajorTextContent");
    elementTC.textContent = strMessageTC;

    //INNERHTML
    var strMessageIH = "I Like t' watch football. Th' <strong>New England Patriots</strong> are me fav'rit crew"
    var elementIH = document.getElementById("miFactInnerHTML");
    elementIH.innerHTML = strMessageIH;

    //DOCUMENT WRITE
    //document.write("ARRR, Mi name be Captain Zaid Saeed.")
    
    //Could not figure out how to showcase document.write on same file

}); // end of $(document).ready()
