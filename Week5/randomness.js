/**************************************
	TITLE: randomness.js
	AUTHOR: Zaid Saeed
	CREATE DATE: 9/17/18
	PURPOSE: Week 5 Lab
	LAST MODIFIED ON: 
	LAST MODIFIED BY: Zaid Saeed
	MODIFICATION HISTORY:
***************************************/

$(document).ready(function () {
	
	function Crew(strRank, strID) {
		
		this.strRank = strRank;
		this.elementOutput = document.getElementById(strID);
		
		this.secretNumber;
		
		this.Guess = function() {
			var intHigh = 10;
			var intLow = 1;
			
			this.intGuess = Math.floor((intHigh - intLow + 1) * Math.random() + intLow);
		};
		
		this.speak = function(strSpeak) {
			this.elementOutput.innerHTML += "<br>" + strSpeak;
		};
		
	};
	
	//initializing Ship Members
	var Captain = new Crew("Captain", "captain");
	var Crew1 = new Crew("Navigator", "crew1");
	var Crew2 = new Crew("Co-Captain", "crew2");
	var Crew3 = new Crew("Warrior", "crew3");
	var Crew4 = new Crew("Tourist", "crew4");
	
	Captain.speak("Lets play a guessing game.");
	
	//Initialize Guesses
	Captain.Guess();
	Crew1.Guess();
	Crew2.Guess();
	Crew3.Guess();
	Crew4.Guess();
	
	//Store if Crew member guessed right
	var crewGuesses = [];
	
	//Begin crew members saying guesses
	Crew1.speak("My name is Luigi and I am a " + Crew1.strRank + " ! My guess is " + Crew1.intGuess + ".");
	
	Crew2.speak("My name is Mario and I am the " + Crew2.strRank + " ! My guess is " + Crew2.intGuess + ".");
	
	Crew3.speak("My name is Barbosa and I strike fear into the enemies of this ship. My rank would be " + Crew3.strRank + " My guess would be " + Crew3.intGuess + ".");
	
	Crew4.speak("My name is Jim. I got lost. I guess I'm a " + Crew4.strRank + " My guess is " + Crew4.intGuess + ".");
	
	//Compare Crew Guesses with Captain Guess
	//If equal push true to array
	//If not push false to array
	if (Crew1.intGuess == Captain.intGuess)
		crewGuesses.push(Crew1.strRank);
	else
		crewGuesses.push(false);
	
	if (Crew2.intGuess == Captain.intGuess)
		crewGuesses.push(Crew2.strRank);
	else
		crewGuesses.push(false);
	
	if (Crew3.intGuess == Captain.intGuess)
		crewGuesses.push(Crew3.strRank);
	else
		crewGuesses.push(false);
	
	if (Crew4.intGuess == Captain.intGuess)
		crewGuesses.push(Crew4.strRank);
	else
		crewGuesses.push(false);
	
	//variable that changes if a guess was correct
	var correct = false;
	
	//loop through array to see if any guess was true and see what rank was true
	for (var i = 0; i < crewGuesses.length; i++) {
		if (crewGuesses[i] != false) {
			Captain.speak("ARGH. The " + crewGuesses[i] + " got the guess right. The Guess was " + Captain.intGuess + ".");
			correct = true;
		}
	}
	
	//if no guess was correct than print what the correct guess was
	if (!correct) {
		Captain.speak("ARGH. Nobody guessed correctly. The Guess was " + Captain.intGuess + ".");
	}
	
	
});