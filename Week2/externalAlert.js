/**************************************
 TITLE: externalAlert.js
 AUTHOR: Zaid Saeed
 CREATE DATE: 08/23/18
 PURPOSE: To say hi to the webpage viewer with pirate's tongue
 LAST MODIFIED ON: 08/23/18
 LAST MODIFIED BY: Zaid Saeed
 MODIFICATION HISTORY:
 08/23/18 Original Build
***************************************/

// The $ is the jQuery object
// "document" is the document object
// ready is a method of the jQuery object
// function creates an anonymous function to contain the code that should run
// In English, when the DOM has finished loading, execute the code in the function.
// See pages 312-313 of the text for details.
$(document).ready(function(){
			
	// Pop up a window that says "Here's a javascript test file!"
	alert("Ahoy World. Tis me website.");
				
}); // end of $(document).ready()