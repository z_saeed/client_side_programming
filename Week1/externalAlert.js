/**************************************
 TITLE: jQuery							<----- Update the file name
 AUTHOR: ZAID SAEED			<----- *Your* name should be here
 CREATE DATE: 08/20/2018
 PURPOSE: To develop good software engineeering skills		<----- Also update the purpose of the file
 LAST MODIFIED ON: 08/20/2018
 LAST MODIFIED BY: ZAID SAEED
 MODIFICATION HISTORY:
 08/20/2018 - Initial Creation
***************************************/

// The $ is the jQuery object
// "document" is the document object
// ready is a method of the jQuery object
// function creates an anonymous function to contain the code that should run
// In English, when the DOM has finished loading, execute the code in the function.
// See pages 312-313 of the text for details.
$(document).ready(function(){
			
	// Pop up a window that says "Here's a javascript test file!"
	alert("This is a pop up window that says, this is a pop up window that says....");
				
}); // end of $(document).ready()