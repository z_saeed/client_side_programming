/**************************************
	TITLE: moreifs.js
	AUTHOR: Zaid Saeed
	CREATE DATE: 9/24
	PURPOSE: More Ifs lab
	LAST MODIFIED ON: 
	LAST MODIFIED BY: Zaid Saeed
	MODIFICATION HISTORY:
***************************************/

$(document).ready(function() {
	
	//Current latitude and longitude
	var intCurrentLatitude = 0;
	var intCurrentLongitude = 0;
	
	//Destination latitude and longitude
	var intDestinationLatitude = 0;
	var intDestinationLongitude = 0;
	
    var strExample = "1541";
	
	intCurrentLatitude = prompt("What is your current Latitude?", strExample);
	intCurrentLongitude = prompt("What is your current Longitude?", strExample);
	
	intDestinationLatitude = prompt("What is your destination Latitude?", strExample);
	intDestinationLongitude = prompt("What is your destination Longitude?", strExample);
	
	document.getElementById("userInput").textContent = "Current Latitude: " + intCurrentLatitude + " Current Longitude: " + intCurrentLongitude + " Destination Latitude: " + intDestinationLatitude + " Destination Longitude: " + intDestinationLongitude;
	
	//If/Else construct
	var ifElseOutput = document.getElementById("ifElse");
	
	if(intCurrentLatitude <= intDestinationLatitude && intCurrentLongitude <= intDestinationLongitude) {
		ifElseOutput.textContent = "Head North East";
	}else if(intCurrentLatitude <= intDestinationLatitude && intCurrentLongitude >= intDestinationLongitude){
		ifElseOutput.textContent = "Head North West";
	}else if(intCurrentLatitude >= intDestinationLatitude && intCurrentLongitude >= intDestinationLongitude) {
		ifElseOutput.textContent = "Head South West";
	} else if (intCurrentLatitude >= intDestinationLatitude && intCurrentLongitude <= intDestinationLongitude) {
		ifElseOutput.textContent = "Head South East";
	} else {
		ifElseOutput.textContent = "Land Ho!";
	}
	
	//SwitchCase construct
	var caseSwitchOutput = document.getElementById("caseSwitch");
	switch(true) {
		case (intCurrentLatitude <= intDestinationLatitude && intCurrentLongitude <= intDestinationLongitude):
			caseSwitchOutput.textContent = "Head North East";
			break;
		case (intCurrentLatitude <= intDestinationLatitude && intCurrentLongitude >= intDestinationLongitude):
			caseSwitchOutput.textContent = "Head North West";
			break;
		case (intCurrentLatitude >= intDestinationLatitude && intCurrentLongitude >= intDestinationLongitude):
			caseSwitchOutput.textContent = "Head South West";
			break;
		case (intCurrentLatitude >= intDestinationLatitude && intCurrentLongitude <= intDestinationLongitude):
			caseSwitchOutput.textContent = "Head South East";
			break;
		default:
			caseSwitchOutput.textContent = "Land Ho!";
			break;
	}
	
});