/**************************************
	TITLE: iffy.js
	AUTHOR: Zaid Saeed
	CREATE DATE: 9/24
	PURPOSE: iffy lab
	LAST MODIFIED ON: 
	LAST MODIFIED BY: Zaid Saeed
	MODIFICATION HISTORY:
***************************************/

$(document).ready(function() {
	var strAnswer = "";
	strAnswer = prompt("Did you arrive to port on time?")
	
	strAnswer = strAnswer.toUpperCase();
	
	var outputElement = document.getElementById("response");
	
	if (strAnswer === "YES")
		outputElement.textContent = "Yee have earned extra doubloons.";
	else if (strAnswer === "NO")
		outputElement.textContent = "ARGH, walk the plank!";
	else
		outputElement.textContent = "Argh, You speaking a language I don't know. Was that a yes or no?"
});