/**************************************
    TITLE: functions.js
    AUTHOR: Zaid Saeed
    CREATE DATE: 9/9/18
    PURPOSE: showcase Functions
    LAST MODIFIED ON: 9/9
    LAST MODIFIED BY: Zaid Saeed
    MODIFICATION HISTORY: Init
***************************************/

$(document).ready(function () {
   
    var strFirstMsg1 = "Argh I see eh enemy's ship! It be ";
    var strFirstMsg2 = " miles away. We must attack!";
    var intFirst = 2;
    var strId1 = "firstMessage";
    captainDialog(strId1, strFirstMsg1, strFirstMsg2, intFirst);
    
    var strSecondMsg1 = "Argh the enemy ship be getting closer! Now it is ";
    var strSecondMsg2 = " mile away. We must continue to move forward!";
    var intSecond = 1;
    var strId2 = "secondMessage";
    captainDialog(strId2, strSecondMsg1, strSecondMsg2, intSecond);
    
    var strThirdMsg1 = "We are only ";
    var strThirdMsg2 = " feet away. PREPARE FOR BATTLE!";
    var intThird = 4000;
    var strId3 = "thirdMessage";
    captainDialog(strId3, strThirdMsg1, strThirdMsg2, intThird);
    
    var strFourthMsg1 = "The enemy ship was further than I thought, but now it is ";
    var strFourthMsg2 = " feet away. That ship better have good treasure!";
    var intFourth = 1500;
    var strId4 = "fourthMessage";
    captainDialog(strId4, strFourthMsg1, strFourthMsg2, intFourth);
    
    var strFifthMsg1 = "The ship is only ";
    var strFifthMsg2 = " feet away. Argh, now I can see it be looted already.";
    var intFifth = 300;
    var strId5 = "fifthMessage";
    captainDialog(strId5, strFifthMsg1, strFifthMsg2, intFifth);
    
});

//Parameters are in the following order:
//div ID, first string, second string, and the int
function captainDialog(strId, str1, str2, int1) {
    var strFull = str1 + int1 + str2; //concatenating the string
    var write = document.getElementById(strId); //getting the div to write in
    write.textContent = strFull; //Writing the string into the div
}